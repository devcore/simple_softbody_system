## **REQUIREMENTS** ##
Microsoft Visual Studio 2012 or newer.

## **SUMMARY** ##
An implementation of a simple [Soft Body](https://en.wikipedia.org/wiki/Soft_body_dynamics) System. Features soft body construction from mesh vertex data utilising distance constraints between point masses. Dynamic soft normal computation using compute shader and constraint stress level visualization.

## **NOTICE** ##
Copyright 2016 Andrius Jefremovas

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.