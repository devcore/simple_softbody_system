/*--------------------------------------------------------\
| File: Point.h                                           |
|x-------------------------------------------------------x|
| Details: Defines attributes and integration method for  |
| a single point mass.                                    |
|x-------------------------------------------------------x|
| Author:                              Andrius Jefremovas |
| Description:                          Point mass object |
\--------------------------------------------------------*/

#pragma once

#include <graphics_framework.h>

struct Point
{
    Point();
    Point(const glm::vec3 &position);
    Point(const glm::vec3 &position, float invmass);
    ~Point();
    // Increases acceleration a = f / m
    void AddForce(const glm::vec3 &force);
    // Moves point based on gravity and accumulated forces
    void Integrate(const float delta_time);
    // Properties
    glm::vec3 m_pos;  // Position
    glm::vec3 m_vel;  // Velocity
    glm::vec3 m_acc;  // Acceleration
    float m_invmass;  // Inverse mass
    bool m_active;    // Enable|Disable Integration|AddForce
};
