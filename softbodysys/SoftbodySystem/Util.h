#pragma once

#if DEBUG | _DEBUG
#   define DBG_ASSERT(x) { if (!(x)){ __debugbreak(); }; }
#else
#   define DBG_ASSERT(x) { }
#endif

