/*--------------------------------------------------------\
| File: main.cpp                                          |
|x-------------------------------------------------------x|
| Details: An inplementation of a simple soft body simu-  |
| lation. Utilises mesh vertex data to create distance    |
| contraints between point masses to simulate a softbody. |
| Renders points, lines, faces and computes soft normals. |
| Also displays contraint stress level by changing color. |
|x-------------------------------------------------------x|
| CONTROLS:                                               |
| SPACEBAR - Simulation Start/Pause                       |
| TAB      - Changes camera type                          |
| Alt      - Unlock/Locks mouse                           |
| ESC      - Closes application                           |
| 1-4      - Swap softbodies                              |
| F1       - Toggles point display                        |
| F2       - Toggles line display                         |
| F3       - Toggles face display                         |
| F5       - Increases hook force constant                |
| F6       - Decreases hook force constant                |
| F7       - Increases hook damp constant                 |
| F8       - Decreases hook damp constant                 |
|x-------------------------------------------------------x|
| FREE Camera                                             |
| WASD QE      - Movement                                 |
| Mouse/Arrows - Rotation                                 |
| Shift        - HOLD to increase movement speed          |
|x-------------------------------------------------------x|
| ARC-BALL Camera:                                        |     _____
| WASD/Mouse - Rotation                                   |     \   /
| QE/Scroll  - Zoom/Distance                              |     /   \
| Shift      - HOLD to increase speed of zooming          |    /  A  \
|x-------------------------------------------------------x|   /  / \  \
| Date:                                         July 2016 |  /  /   \  \
| Author:                              Andrius Jefremovas | /  /_____\  \
| Description: Entry point - creates system and links it. | \___________/
\--------------------------------------------------------*/
#include <graphics_framework.h>
#include "CameraSystem.h"
#include "SoftbodySystem.h"

CameraSystem *cam = nullptr;
SoftbodySystem *softsys = nullptr;

bool Initialise()
{
    cam = new CameraSystem();
    softsys = new SoftbodySystem();
    return true;
}

bool Update(const float delta_time)
{
    cam->ProcessInput(delta_time);
    softsys->Update(delta_time);
    return true;
}

bool Render(const float time)
{
    softsys->Render(cam->GetActiveCamera());
    return true;
}

void CleanUp()
{
    delete cam;
    delete softsys;
}

int main()
{
    // Initialise rendering framework
    graphics_framework::app application;
    // Link the framework to the simulation
    application.set_initialise(Initialise);
    application.set_update(Update);
    application.set_render(Render);
    application.run();
    CleanUp();

    return 0;
}