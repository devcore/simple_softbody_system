/*--------------------------------------------------------\
| File: Constraints.h                                     |
|x-------------------------------------------------------x|
| Details: Spring contraints used to keep apart two point |
| masses. Features linear, square and angular constraints |
|x-------------------------------------------------------x|
| Author:                              Andrius Jefremovas |
| Description:               Point mass spring contraints |
\--------------------------------------------------------*/
#pragma once

#include "Point.h"

const enum CONSTRAINT_TYPES
{
    LINEAR_CONSTRAINT,
    SQUARE_CONSTRAINT,
    ANGULAR_CONSTRAINT
};

/* Constrains particle 1 to particle 2 ---------------------------------------*/
struct Constraint
{
    Constraint(Point *point1, Point *point2);
    virtual ~Constraint() = 0;
    // Compute constraint and apply force
    virtual void Update(const float delta_time) = 0;
    // Returns scale remaining to rest
    virtual float GetStress() = 0;
    // Connected points
    Point *m_point1, *m_point2;
    // Hookean Constants
    static float m_khook, m_kdamp;
    static glm::uint num_constraints;
};

/* Linear distance constraint - maintains distance with linear force ---------*/
struct LinearConstraint : public Constraint
{
    LinearConstraint(Point *point1, Point *point2);
    // Calculate distance constraint and apply force
    void Update(const float delta_time);
    // Returns previous frame's stretch distance
    float GetStress();
private:
    // Distance between points to maintain
    float m_rest_distance;
    // Distance left to reach rest
    float m_stretch_distance;
};

/* Squared distance constraint - maitains distance with angular force --------*/
struct SquareConstraint : public Constraint
{
    SquareConstraint(Point *point1, Point *point2);
    // Calculate squared distance constraint and apply force
    void Update(const float delta_time);
    // Returns previous frame's stretch distance
    float GetStress();
private:
    // Distance between points to maintain
    float m_rest_distance2;
    // Distance left to reach rest
    float m_stretch_distance;
};

/* Angular constraint - maintains angle --------------------------------------*/
struct AngularConstraint
{

};