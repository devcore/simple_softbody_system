/*--------------------------------------------------------\
| File: SoftBody.h                                        |
|x-------------------------------------------------------x|
| Details: Utilises mesh vertex data as point masses and  |
| contraints between them to form a softbody object. Uses |
| multiple shaders to represent points, lines, faces and  |
| computes normals using a compute shader. 3ds format     |
| yields best results.                                    |
|x-------------------------------------------------------x|
| Author:                              Andrius Jefremovas |
| Description:         Contructs softbody from mesh data. |
\--------------------------------------------------------*/

#pragma once

#include <graphics_framework.h>
#include "Constrainsts.h"

struct SoftBody
{
    SoftBody(const std::string &filename, float scale);
    ~SoftBody();
    // Add acceleration to each point
    void AddForce(const glm::vec3 &force);
    // Add angular acceleration to each point based on the center point
    void SoftBody::AddSpinForce(const float force, const glm::vec3 &axis);
    // Solve constraints and integrate
    void Update(const float delta_time,
                const graphics_framework::effect *eff_accum_normals);
    // Render points, lines and/or faces
    void Render(const graphics_framework::camera *cam,
                const graphics_framework::effect *eff_points,
                const graphics_framework::effect *eff_lines,
                const graphics_framework::effect *eff_faces,
                const graphics_framework::material *mat,
                const graphics_framework::directional_light *light);

    // Render modes
    bool m_render_points, m_render_lines, m_render_faces;
    // Return soft body point masses
    std::vector<Point>* GetSoftBodyPoints(){ return &m_points; }
private:
    // Create a constraint between two points | Linear Squared or Angular
    bool AddConstraint(Point *p1, Point *p2, const CONSTRAINT_TYPES &type);

    // Point, line and face buffers
    GLuint m_vao[3], m_vbo[4];
    glm::uint num_verts;
    // Softbody point, constraint and face buffers
    std::vector<Point> m_points;
    //std::vector<Point> m_shell_points;
    std::vector<Constraint*> m_constraints;
    std::vector<GLuint> m_indices;
};