/* File: Constraints.cpp
--------------------------------------------------------------------------------
For details see Constraints.h */

#include "Constrainsts.h"
#include "Util.h"

using namespace glm;

// Initiliase constants
float Constraint::m_khook{ 1.0f };
float Constraint::m_kdamp{ 0.05f };

/* -----------------------------------------------------------------------------
            BASE CONSTRAINT
----------------------------------------------------------------------------- */
Constraint::Constraint(Point *p1, Point *p2)
{
    // Sanity checks
    DBG_ASSERT(p1 != p2);
    DBG_ASSERT(p1 != nullptr);
    DBG_ASSERT(p2 != nullptr);
    // Save pointers to the points
    m_point1 = p1;
    m_point2 = p2;
}

Constraint::~Constraint() { /* Virtual */ }

/* ----------------------------------------------------------------------------- 
            LINEAR CONSTRAINT 
----------------------------------------------------------------------------- */
LinearConstraint::LinearConstraint(Point *p1, Point *p2) : Constraint(p1, p2) 
{ 
    // Calculate the distance and save it as the rest distance
    m_rest_distance = length(p1->m_pos - p2->m_pos);
    DBG_ASSERT(m_rest_distance);
}

void LinearConstraint::Update(const float delta_time)
{
    /*
      F = -k(|d|-r)(d/|d|)-bv
      d -> distance
      r -> rest distance
      v -> relative velocity
      b -> coefficient of damping
    */
    DBG_ASSERT(length2(m_point1->m_pos) < 1000000.0f);
    DBG_ASSERT(length2(m_point2->m_pos) < 1000000.0f);
    // Distance between points
    const vec3 dist = m_point2->m_pos - m_point1->m_pos;
    // Scalar distance
    float curr_dist = length(dist + epsilon<float>());
    // Stretch distance
    m_stretch_distance = curr_dist - m_rest_distance - epsilon<float>();
    vec3 normal = normalize(dist);
    // Relative velocity
    vec3 rel_vel = m_point2->m_vel - m_point1->m_vel;
    float damping = dot(normal, rel_vel);
    // Compute hook force
    vec3 fhook = normal * m_stretch_distance * m_khook;
    // Compute damping force
    vec3 fdamp = normal * damping * m_kdamp;
    // Apply hook forces to points
    DBG_ASSERT(length2(fhook) < FLT_MAX);
    DBG_ASSERT(length2(fdamp) < FLT_MAX);
    vec3 force = fhook + fdamp;
    m_point1->AddForce( force);
    m_point2->AddForce(-force);
}

float LinearConstraint::GetStress()
{
#if 1
    return m_stretch_distance;
#else
    return 0.0f;
#endif
}

/* -----------------------------------------------------------------------------
            SQUARED CONSTRAINT
----------------------------------------------------------------------------- */
SquareConstraint::SquareConstraint(Point *p1, Point *p2) : Constraint(p1, p2) 
{ 
    // Calculate the distance and save it as the rest distance
    m_rest_distance2 = length2(p1->m_pos - p2->m_pos);
    DBG_ASSERT(m_rest_distance2);
}

void SquareConstraint::Update(const float delta_time)
{
    /*
      F = -k(|d|-r)(d/|d|)-bv
      d -> distance
      r -> rest distance
      v -> relative velocity
      b -> coefficient of damping
    */
    DBG_ASSERT(length2(m_point1->m_pos) < 1000000.0f);
    DBG_ASSERT(length2(m_point2->m_pos) < 1000000.0f);
    // Distance between points
    const vec3 dist = m_point2->m_pos - m_point1->m_pos;
    // Scalar distance squared
    float curr_dist = length2(dist + epsilon<float>());
    // Stretch distance
    m_stretch_distance = curr_dist - m_rest_distance2 - epsilon<float>();
    vec3 normal = normalize(dist);
    // Relative velocity
    vec3 rel_vel = m_point2->m_vel - m_point1->m_vel;
    float damping = dot(normal, rel_vel);
    // Compute hook force
    vec3 fhook = normal * m_stretch_distance * m_khook;
    // Compute damping force
    vec3 fdamp = normal * damping * m_kdamp;
    // Apply hook forces to points
    DBG_ASSERT(length2(fhook) < FLT_MAX);
    DBG_ASSERT(length2(fdamp) < FLT_MAX);
    vec3 force = fhook + fdamp;
    m_point1->AddForce( force);
    m_point2->AddForce(-force);
}

float SquareConstraint::GetStress()
{
#if 1
    return m_stretch_distance;
#else
    return 1.0f;
#endif
}

/* -----------------------------------------------------------------------------
            ANGULAR CONSTRAINT
----------------------------------------------------------------------------- */