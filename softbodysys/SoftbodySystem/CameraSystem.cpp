/* File: CameraSystem.cpp
--------------------------------------------------------------------------------
For details see CameraSystem.h */

#include "CameraSystem.h"
#include "InputHandler.h"
#include <FreeImage\FreeImage.h>

using namespace glm;
using namespace graphics_framework;

CameraSystem::CameraSystem()
{
    std::cerr << "CameraSystem created." << std::endl;
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    m_lock_mouse = false;
    m_active_cam = CAMERA_TYPES::ARC_CAM;
    // Initialise cursor position at zero
    m_cursor_x = m_cursor_y = 0.0;
    m_delta_x = m_delta_y = 0.0;
    // Hide cursor arrow
    glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    // Get current position of cursor
    glfwGetCursorPos(renderer::get_window(), &m_cursor_x, &m_cursor_y);
    // Calculate aspect and screen ratios
    m_aspect = static_cast<float>(renderer::get_screen_width()) /
        static_cast<float>(renderer::get_screen_height());
    m_ratio_width = quarter_pi<float>() /
        static_cast<float>(renderer::get_screen_width());
    m_ratio_height = (quarter_pi<float>() *
        (static_cast<float>(renderer::get_screen_height()) /
        static_cast<float>(renderer::get_screen_width()))) /
        static_cast<float>(renderer::get_screen_height());
    //Free camera initilisation
    m_free_cam = free_camera();
    // Default the camera at the centre
    m_free_cam.set_position(vec3(10.0f));
    m_free_cam.set_target(vec3(0.0f));
    // Set field of view to ~80 degrees, aspect, near plane, far plane 
    m_free_cam.set_projection(pi<float>()*0.45f, m_aspect, 2.414f, 10000.0f);
    // Arc ball camera initilisation
    m_arc_cam = arc_ball_camera();
    m_arc_cam.set_position(vec3(0.0f));
    m_arc_cam.set_target(vec3(0.0f));
    m_arc_cam.set_distance(12.0f);
    m_arc_cam.set_projection(pi<float>()*0.45f, m_aspect, 2.414f, 10000.0f);
    m_arc_cam.rotate(-half_pi<float>()*0.3f, half_pi<float>()*0.3f);
    glfwSetScrollCallback(renderer::get_window(), ScrollWheel);
}

CameraSystem::~CameraSystem()
{
    std::cerr << "CameraSystem Destroyed." << std::endl;
}

void CameraSystem::ProcessInput(float dt)
{
    switch (m_active_cam)
    {
    case CameraSystem::FREE_CAM:
        UpdateFreeCam(dt);
        break;
    case CameraSystem::ARC_CAM:
        UpdateArcCam(dt);
        break;
    default:
        std::cerr << "<file:CameraSys> Uknown cam_type!" << std::endl;
        _CrtDbgBreak();
        break;
    }
    // Toggle cursor lock
    if (KeyPressOnce(GLFW_KEY_LEFT_ALT)) 
    {
        if (m_lock_mouse = !m_lock_mouse)
        {
            glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        }
        else
        {
            glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        }
    }
    // Toggle active camera
    if (KeyPressOnce(GLFW_KEY_TAB))
    {
        m_active_cam = (m_active_cam == FREE_CAM) ? ARC_CAM : FREE_CAM;
    }
    m_delta_x = 0.0;
    m_delta_y = 0.0;
}

void CameraSystem::UpdateFreeCam(float dt)
{
    // Get new cursor position
    glfwGetCursorPos(renderer::get_window(), &m_current_x, &m_current_y);
    // If the mouse is active - track its position
    if (!m_lock_mouse)
    {
        // Get the amount the cursor has moved
        m_delta_x = (m_current_x - m_cursor_x) * m_ratio_width;
        m_delta_y = (m_current_y - m_cursor_y) * m_ratio_height;
        // Keep the old position
        m_cursor_x = m_current_x;
        m_cursor_y = m_current_y;
    }
    // Camera rotation input 
    if (KeyPressRepeat(GLFW_KEY_LEFT))  { m_delta_x -= 2.0f * dt; }
    if (KeyPressRepeat(GLFW_KEY_RIGHT)) { m_delta_x += 2.0f * dt; }
    if (KeyPressRepeat(GLFW_KEY_UP))    { m_delta_y -= 2.0f * dt; }
    if (KeyPressRepeat(GLFW_KEY_DOWN))  { m_delta_y += 2.0f * dt; }
    // Camera movement input
    vec3 translation(0.0f, 0.0f, 0.0f);
    if (KeyPressRepeat('W')) { translation.z += 10.0f * dt; }
    if (KeyPressRepeat('S')) { translation.z -= 10.0f * dt; }
    if (KeyPressRepeat('A')) { translation.x -= 10.0f * dt; }
    if (KeyPressRepeat('D')) { translation.x += 10.0f * dt; }
    if (KeyPressRepeat('Q')) { translation.y -= 10.0f * dt; }
    if (KeyPressRepeat('E')) { translation.y += 10.0f * dt; }
    if (KeyPressRepeat(GLFW_KEY_LEFT_SHIFT)) { translation *= 20.0f; }
    // Update camera using the input
    m_free_cam.rotate((float)(m_delta_x), -((float)(m_delta_y)));
    m_free_cam.move(translation);
    m_free_cam.update(dt);
}

void CameraSystem::UpdateArcCam(float dt)
{
    // Get new cursor position
    glfwGetCursorPos(renderer::get_window(), &m_current_x, &m_current_y);
    float translation{ 0.0f };
    // If the mouse is active - track its position
    if (!m_lock_mouse)
    {
        // Get the amount the cursor has moved
        m_delta_x = (m_current_x - m_cursor_x) * m_ratio_width;
        m_delta_y = (m_current_y - m_cursor_y) * m_ratio_height;
        // Keep the old position
        m_cursor_x = m_current_x;
        m_cursor_y = m_current_y;
        translation += static_cast<float>(scroll_wheel);
    }
    // Camera movement input
    if (KeyPressRepeat('W')) { m_delta_y -= 2.0f * dt; }
    if (KeyPressRepeat('S')) { m_delta_y += 2.0f * dt; }
    if (KeyPressRepeat('A')) { m_delta_x -= 2.0f * dt; }
    if (KeyPressRepeat('D')) { m_delta_x += 2.0f * dt; }
    if (KeyPressRepeat('Q')) { translation += 15.0f * dt; }
    if (KeyPressRepeat('E')) { translation -= 15.0f * dt; }
    if (KeyPressRepeat(GLFW_KEY_LEFT_SHIFT)) { translation *= 20.0f; }
    // Update camera using the input
    m_arc_cam.rotate(((float)(m_delta_y)), (float)(m_delta_x));
    m_arc_cam.move(translation);
    m_arc_cam.update(dt);
    // Reset scroll wheel input
    scroll_wheel = 0.0f;
}

const camera* CameraSystem::GetActiveCamera()
{
    switch (m_active_cam)
    {
    case CameraSystem::FREE_CAM:
        return &m_free_cam;
        break;
    case CameraSystem::ARC_CAM:
        return &m_arc_cam;
        break;
    default:
        std::cerr << "<file:CameraSys> Uknown cam_type!" << std::endl;
        // Break in debug mode
#if DEBUG | _DEBUG
        _CrtDbgBreak();
#endif
        // Set free cam as default and return it
        m_active_cam = CAMERA_TYPES::FREE_CAM;
        return &m_free_cam;
        break;
    }
}