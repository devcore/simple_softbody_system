/* File: SoftbodySystem.cpp
--------------------------------------------------------------------------------
For details see SoftbodySystem.h */

#include "SoftbodySystem.h"
#include "InputHandler.h"

using namespace glm;
using namespace graphics_framework;

SoftbodySystem::SoftbodySystem()
{
    // Create shader program
    m_bv_eff.add_shader("..\\Resources\\shaders\\simple_texture.vert", GL_VERTEX_SHADER);
    m_bv_eff.add_shader("..\\Resources\\shaders\\simple_texture.frag", GL_FRAGMENT_SHADER);
    m_bv_eff.build();
    renderer::bind(m_bv_eff);
    glUniform1f(m_bv_eff.get_uniform_location("opacity"), 1.0f);
    // Simple grid texture
    m_bv_tex = texture("..\\Resources\\textures\\blueprint.png");
    // Create inverse cube geomtry to represent bounding volume
    m_bv_mesh = mesh(geometry_builder::create_inversebox());
    m_bv_mesh.get_transform().scale = vec3(10.0f);
    // Create soft body graphics resources
    m_sb_points.add_shader("..\\resources\\shaders\\softbody_points.vert", GL_VERTEX_SHADER);
    m_sb_points.add_shader("..\\resources\\shaders\\softbody_points.geom", GL_GEOMETRY_SHADER);
    m_sb_points.add_shader("..\\resources\\shaders\\softbody_points.frag", GL_FRAGMENT_SHADER);
    m_sb_points.build();
    m_sb_lines.add_shader("..\\resources\\shaders\\softbody_lines.vert", GL_VERTEX_SHADER);
    m_sb_lines.add_shader("..\\resources\\shaders\\softbody_lines.frag", GL_FRAGMENT_SHADER);
    m_sb_lines.build();
    m_sb_quads.add_shader("..\\resources\\shaders\\softbody_faces.vert", GL_VERTEX_SHADER);
    m_sb_quads.add_shader("..\\resources\\shaders\\softbody_faces.frag", GL_FRAGMENT_SHADER);
    m_sb_quads.build();
    m_accum_normals.add_shader("..\\resources\\shaders\\accum_normals.comp", GL_COMPUTE_SHADER);
    m_accum_normals.build();
    m_sb_mat = material(vec4(0.3f), vec4(0.8f), vec4(1.0f, 0.7f, 0.1f, 1.0f), 10.0f);
    // Directional light source for all of the objects
    m_light = directional_light();
    m_light.set_ambient_intensity(vec4(0.5f, 0.5f, 0.5f, 1.0f));
    m_light.set_light_colour(vec4(1.0f, 0.7f, 0.1f, 1.0f));
    m_light.set_direction(vec3(1.0f, 1.0f, 1.0f));
    // Default simulation
    m_simulate = false;
    m_softbody = new SoftBody("..\\Resources\\models\\cube.3ds", 2.0f);
    Constraint::m_khook = 2.0f;
    Constraint::m_kdamp = 0.15f;
    m_current_scene = 1;
}

SoftbodySystem::~SoftbodySystem() 
{
    //if (m_softcube) delete m_softcube;
    if (m_softbody) delete m_softbody;
}

void SoftbodySystem::Update(const float delta_time)
{
    if (KeyPressOnce(GLFW_KEY_SPACE)) { m_simulate = !m_simulate; }
    // Softbody setups
    if (KeyPressOnce('1')) { NewScene(1); }
    if (KeyPressOnce('2')) { NewScene(2); }
    if (KeyPressOnce('3')) { NewScene(3); }
    if (KeyPressOnce('4')) { NewScene(4); }
    // Change Render mode
    if (KeyPressOnce(GLFW_KEY_F1)) { m_softbody->m_render_points = !m_softbody->m_render_points; }
    if (KeyPressOnce(GLFW_KEY_F2)) { m_softbody->m_render_lines = !m_softbody->m_render_lines; }
    if (KeyPressOnce(GLFW_KEY_F3)) { m_softbody->m_render_faces = !m_softbody->m_render_faces; }
    // Constraint strength
    if (KeyPressOnce(GLFW_KEY_F5)) { Constraint::m_khook += 1.0f; std::clog << "khook: " << Constraint::m_khook << std::endl; }
    if (KeyPressOnce(GLFW_KEY_F6)) { Constraint::m_khook -= 1.0f; std::clog << "khook: " << Constraint::m_khook << std::endl; }
    if (KeyPressOnce(GLFW_KEY_F7)) { Constraint::m_kdamp += 0.005f; std::clog << "kdamp: " << Constraint::m_kdamp << std::endl; }
    if (KeyPressOnce(GLFW_KEY_F8)) { Constraint::m_kdamp -= 0.005f; std::clog << "kdamp: " << Constraint::m_kdamp << std::endl; }
    // Continue only if the simulation is running
    if (!m_simulate) { return; }
    // Softbody movement
    vec3 force{ 0.0f, 0.0f, 0.0f };
    const float move_speed{ 0.5f }, rot_speed{ 0.25f };
    if (KeyPressRepeat(GLFW_KEY_UP))    { force += vec3(0, 0, -move_speed); }
    if (KeyPressRepeat(GLFW_KEY_DOWN))  { force += vec3(0, 0,  move_speed); }
    if (KeyPressRepeat(GLFW_KEY_LEFT))  { force += vec3(-move_speed, 0, 0); }
    if (KeyPressRepeat(GLFW_KEY_RIGHT)) { force += vec3( move_speed, 0, 0); }
    if (KeyPressRepeat(GLFW_KEY_KP_1))  { force += vec3(0,  move_speed, 0); }
    if (KeyPressRepeat(GLFW_KEY_KP_0))  { force += vec3(0, -move_speed, 0); }
    if (KeyPressRepeat(GLFW_KEY_KP_8))  { m_softbody->AddSpinForce(rot_speed, vec3(-1.0f, 0.0f, 0.0f)); }
    if (KeyPressRepeat(GLFW_KEY_KP_2))  { m_softbody->AddSpinForce(rot_speed, vec3( 1.0f, 0.0f, 0.0f)); }
    if (KeyPressRepeat(GLFW_KEY_KP_4))  { m_softbody->AddSpinForce(rot_speed, vec3(0.0f, 0.0f,  1.0f)); }
    if (KeyPressRepeat(GLFW_KEY_KP_6))  { m_softbody->AddSpinForce(rot_speed, vec3(0.0f, 0.0f, -1.0f)); }
    if (KeyPressRepeat(GLFW_KEY_KP_7))  { m_softbody->AddSpinForce(rot_speed, vec3(0.0f, -1.0f, 0.0f)); }
    if (KeyPressRepeat(GLFW_KEY_KP_9))  { m_softbody->AddSpinForce(rot_speed, vec3(0.0f,  1.0f, 0.0f)); }
    // Contain soft body inside bounding volume
    std::vector<Point>& points_reff = *m_softbody->GetSoftBodyPoints();
    for (uint i = 0; i < points_reff.size(); ++i)
    for (uint j = 0; j < 3; ++j)
    {
        // Constants
        const float khook{ 20.0f }, kdamp{ 0.5f }, box_size{ 5.0f };
        const vec3 box[3] { vec3(1, 0, 0), vec3(0, 1, 0), vec3(0, 0, 1) };
        // Calculate penetration
        float penetration = dot(points_reff[i].m_pos, box[j]);
        float abs_penetration = abs(penetration);
        // Check if point is outside of the box
        if (abs_penetration > box_size)
        {
            vec3 normal = -box[j] * sign(penetration);
            penetration = abs_penetration - box_size;
            vec3 fhook = normal * penetration * khook;
            float damp = dot(normal, points_reff[i].m_vel);
            vec3 fdamp = normal * damp * kdamp;
            assert(length2(fhook) < FLT_MAX);
            assert(length2(fdamp) < FLT_MAX);
            points_reff[i].AddForce(fhook - fdamp);            
        }
    }
    // Apply forces
    m_softbody->AddForce(force);
    m_softbody->Update(delta_time, &m_accum_normals);
}

void SoftbodySystem::Render(const graphics_framework::camera *cam)
{
    //m_softcube->Render(cam, &m_light);
    if (m_softbody)
    {
        m_softbody->Render(cam, &m_sb_points, &m_sb_lines, 
                                &m_sb_quads, &m_sb_mat, &m_light);
    }
    
    { // Render bounding space
        renderer::bind(m_bv_eff);
        const mat4 V = cam->get_view();
        const mat4 P = cam->get_projection();
        const mat4 M = m_bv_mesh.get_transform().get_transform_matrix();
        const mat4 MVP = P * V * M;
        glUniformMatrix4fv(
            m_bv_eff.get_uniform_location("MVP"), 1, GL_FALSE, value_ptr(MVP));
        renderer::bind(m_bv_tex, 0);
        renderer::render(m_bv_mesh);
    } // !Bounding Space
}

void SoftbodySystem::NewScene(const uint id)
{
    // Pause simulation and delete old softbody
    m_simulate = false;
    delete m_softbody;
    // Initialise new scene 
    switch (id)
    {
    case 1:
        m_softbody = new SoftBody("..\\Resources\\models\\cube.3ds", 2.0f);
        if (m_current_scene != id)
        {
            Constraint::m_khook = 2.0f;
            Constraint::m_kdamp = 0.15f;
        }
        break;
    case 2:
        m_softbody = new SoftBody("..\\Resources\\models\\ico_sphere.3ds", 2.5f);
        if (m_current_scene != id)
        {
            Constraint::m_khook = 1.28f;
            Constraint::m_kdamp = 0.02f;
        }
        break;
    case 3:
        m_softbody = new SoftBody("..\\Resources\\models\\cylinder.3ds", 2.5f);
        if (m_current_scene != id)
        {
            Constraint::m_khook = 2.0f;
            Constraint::m_kdamp = 0.1f;
        }
        break;
    case 4:
        m_softbody = new SoftBody("..\\Resources\\models\\cone.3ds", 2.5f);
        if (m_current_scene != id)
        {
            Constraint::m_khook = 2.0f;
            Constraint::m_kdamp = 0.1f;
        }
        break;
    default:
        // If in debug stop
#if DEBUG | _DEBUG
        __debugbreak();
#endif
        // Else print error, but do no stop.
        std::cerr << "ERROR: No scene Id - " << id << std::endl;
        if (m_softbody) { delete m_softbody; }
        m_softbody = new SoftBody("..\\Resources\\models\\cube.3ds", 2.0f);
        m_current_scene = id;
        m_simulate = false;
        break;
    }
    std::clog << "Contraint K_HOOK: " << Constraint::m_khook << std::endl;
    std::clog << "Contraint K_DAMP: " << Constraint::m_kdamp << std::endl;
    // Save old scene id
    m_current_scene = id;
}