/*--------------------------------------------------------\
| File: SoftbodySystem.h                                  |
|x-------------------------------------------------------x|
| Details: Manages softbodies, creates scene and boundary |
|x-------------------------------------------------------x|
| Author:                              Andrius Jefremovas |
| Description:                       Simulates softbodies |
\--------------------------------------------------------*/
#pragma once

#include "SoftBody.h"

struct SoftbodySystem
{
    SoftbodySystem();
    ~SoftbodySystem();
    void Update(const float delta_time);
    void Render(const graphics_framework::camera *cam);
private:
    void NewScene(const glm::uint id);
    //SoftCube* m_softcube;
    SoftBody* m_softbody;
    bool m_simulate;
    glm::uint m_current_scene;
    // Bounding volume graphics resources
    graphics_framework::effect m_bv_eff;
    graphics_framework::mesh m_bv_mesh;
    graphics_framework::texture m_bv_tex;
    // Softbody graphics resources
    graphics_framework::effect m_sb_points;
    graphics_framework::effect m_sb_lines;
    graphics_framework::effect m_sb_quads;
    graphics_framework::effect m_accum_normals;
    graphics_framework::material m_sb_mat;
    // Scene light source
    graphics_framework::directional_light m_light;
};