/* File: Point.cpp
--------------------------------------------------------------------------------
For details see Point.h */

#include "Point.h"
#include "Util.h"

using namespace glm;

Point::Point() : m_pos(0.0f), m_vel(0.0f), m_invmass(0.0f), m_active(false) { }

Point::Point(const vec3 &position) :
    m_pos(position), m_vel(0.0f), m_invmass(100.0f), m_active(true) { }

Point::Point(const vec3 &position, float invmass) : 
    m_pos(position), m_vel(0.0f), m_invmass(invmass), m_active(true) { }

Point::~Point() { /* Empty */ }

void Point::AddForce(const glm::vec3 &force) 
{ 
    // Don't add force of point is inactive
    if (!m_active) { return; }
    // Sanity check
    DBG_ASSERT(length2(force) < FLT_MAX);
    // Add force a = f / m
    m_acc += force * m_invmass;
}

void Point::Integrate(const float delta_time)
{
    // Don't integrate if point is inactive
    if (!m_active) { return; }
    // Sanity checks
    DBG_ASSERT(length2(m_pos) < FLT_MAX);
    DBG_ASSERT(length2(m_vel) < FLT_MAX);
    DBG_ASSERT(length2(m_acc) < FLT_MAX);
    DBG_ASSERT((m_invmass < 1000000.0f || m_invmass > 0));
    // Add gravity
    m_acc -= vec3(0.0f, 9.82f, 0.0f);
    // Eulerian semi-implicit integration
    m_vel += m_acc * delta_time;
    m_pos += m_vel * delta_time;
    m_vel *= 0.99873f;
    m_acc = vec3(0.0f);
}