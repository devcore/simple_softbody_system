/* File: SoftBody.cpp
--------------------------------------------------------------------------------
For details see SoftBody.h */

#include "SoftBody.h"
#include "Util.h"

using namespace glm;
using namespace graphics_framework;

#define POINTS_SIZE sizeof(vec4)*m_points.size()
#define LINES_SIZE sizeof(vec4)*m_constraints.size()*2
#define FACES_SIZE sizeof(GLuint)*m_indices.size()

SoftBody::SoftBody(const std::string &filename, float scale) : 
m_render_points(false), m_render_lines(true), m_render_faces(true)
{
    // Check if file exist
    assert(graphics_framework::check_file_exists(filename));
    // Create model importer
    Assimp::Importer model_importer;
    // Read in model data
    const aiScene* src = model_importer.ReadFile(
        filename, aiProcess_Triangulate | 
        aiProcess_JoinIdenticalVertices | 
        aiProcess_ValidateDataStructure | 
        aiProcess_FindInvalidData);
    // Check if data exists
    if (!src)
    {
        // Display error
        std::cerr << "ERROR - loading geometry " << filename << std::endl;
        std::cerr << model_importer.GetErrorString() << std::endl;
        // Break if in debug and exit if release
#if DEBUG | _DEBUG
        __debugbreak();
#else
        glfwSetWindowShouldClose(graphics_framework::renderer::get_window(), 1);
#endif
    }
    std::vector<vec4> vertices;
    std::vector<vec4> normals;
    uint vertex_begin = 0;
    // Iterate through each sub-mesh in the model
    for (uint n = 0; n < src->mNumMeshes; ++n)
    {
        // Get the sub-mesh and iterate through all the vertices
        aiMesh* mesh = src->mMeshes[n];
        for (uint i = 0; i < mesh->mNumVertices; ++i)
        {
            // Get position vertex and store it
            const aiVector3t<float> pos = mesh->mVertices[i];
            vertices.push_back(vec4(pos.x, pos.y, pos.z, 0.0f) * scale); 
        }
        // Add normals if they exist
        if (mesh->HasNormals())
        for (uint i = 0; i < mesh->mNumVertices; ++i)
        {
            const aiVector3t<float> norm = mesh->mNormals[i];
            normals.push_back(glm::vec4(norm.x, norm.y, norm.z, 0.0f));
        }
        // If we have face information, then add to index buffer
        if (mesh->HasFaces())
        for (uint f = 0; f < mesh->mNumFaces; ++f)
        {
            const aiFace& face = mesh->mFaces[f];
            for (uint i = 0; i < 3; ++i)
            {
                m_indices.push_back(vertex_begin + face.mIndices[i]);
            }
        }
        vertex_begin += mesh->mNumVertices; // Next sub-mesh starting point
    }
    // Log success
    std::clog << "LOG - softbody: " << filename << " loaded" << "\n" <<
                 "Vertices: " << vertices.size() << "\n" << 
                 "Indices: " << m_indices.size() << "\n";
    // Resize soft body points to vertex amount
    num_verts = vertices.size();
    m_points.resize(num_verts+1);
    m_points[m_points.size()-1] = Point{ vec3(0.0f) };
    for (size_t i = 0; i < vertices.size(); i++)
    {
        m_points[i] = Point{ vec3(vertices[i]) };
        AddConstraint(&m_points.back(), &m_points[i], SQUARE_CONSTRAINT);
    }

    for (size_t i = 0; i < m_indices.size(); i += 3)
    {
        Point *p1{ &m_points[m_indices[i]] };
        Point *p2{ &m_points[m_indices[i+1]] };
        Point *p3{ &m_points[m_indices[i+2]] };

        AddConstraint(p1, p2, LINEAR_CONSTRAINT);
        AddConstraint(p2, p3, LINEAR_CONSTRAINT);
        AddConstraint(p3, p1, LINEAR_CONSTRAINT);
    }
    std::clog << "Number of contraints: " << m_constraints.size() << std::endl;
    // Generate OpenGL buffers
    glGenVertexArrays(3, m_vao);
    glGenBuffers(4, m_vbo);
    // POINT BUFFER
    glBindVertexArray(m_vao[0]);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, POINTS_SIZE, &vertices[0], GL_STREAM_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);
    // LINE BUFFER
    glBindVertexArray(m_vao[1]);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo[1]);
    std::vector<vec4> lines(m_constraints.size()*2);
    for (uint i = 0, j = 0; i < m_constraints.size(); ++i, j += 2)
    {
        lines[j] = vec4(m_constraints[i]->m_point1->m_pos, m_constraints[i]->GetStress());
        lines[j+1] = vec4(m_constraints[i]->m_point2->m_pos, m_constraints[i]->GetStress());
    }
    glBufferData(GL_ARRAY_BUFFER, LINES_SIZE, &lines[0], GL_STREAM_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);
    // FACE BUFFER
    glBindVertexArray(m_vao[0]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vbo[2]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, FACES_SIZE, &m_indices[0], GL_STATIC_DRAW);
    // NORMAL BUFFER
    glBindVertexArray(m_vao[0]);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo[3]);
    glBufferData(GL_ARRAY_BUFFER, normals.size()*sizeof(vec4), &normals[0], GL_STATIC_DRAW);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);
    // Link buffers together for compute shader
    const GLuint ssbos[3] = { m_vbo[0], m_vbo[2], m_vbo[3] };
    glBindBuffersBase(GL_SHADER_STORAGE_BUFFER, 0, 3, ssbos);
}

SoftBody::~SoftBody() 
{
    m_points.clear();
    for (uint i = 0; i < m_constraints.size(); i++)
    {
        delete m_constraints[i];
    }
    m_constraints.clear();
    glDeleteVertexArrays(3, m_vao);
    glDeleteBuffers(4, m_vbo);
}

void SoftBody::Update(const float delta_time, const effect *eff_accum_normals)
{
    // Resolve all of the constraints
    for (uint i = 0; i < m_constraints.size(); i++)
    {
        m_constraints[i]->Update(delta_time);
    }
    m_points[num_verts].m_acc = clamp(m_points[num_verts].m_acc, -100.0f, 100.0f);
    // Integrate points
    for (uint i = 0; i < m_points.size(); i++)
    {
        m_points[i].Integrate(delta_time);
    }
    // Compute soft normals
    renderer::bind(*eff_accum_normals);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo[0]);
    glUniform1ui(eff_accum_normals->get_uniform_location("num_indices"), m_indices.size());
    glDispatchCompute(num_verts, 1, 1);
    glMemoryBarrier(GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT);
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}

void SoftBody::Render(const camera *cam,
                      const effect *eff_points,
                      const effect *eff_lines,
                      const effect *eff_faces,
                      const material *mat,
                      const directional_light *light)
{
    // Store repeatedly used data
    const mat4 V = cam->get_view();
    const mat4 P = cam->get_projection();
    const mat4 M = mat4(1.0f);
    const mat4 MVP = P * V * M;
    const vec3 cam_pos = cam->get_position();
    // Update softbody point mass positions
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo[0]);
    vec4* ptr_mapbuffer = (vec4*)glMapBufferRange(GL_ARRAY_BUFFER, 0, POINTS_SIZE,
                                                  GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT);
    for (uint i = 0; i < m_points.size(); ++i)
    {
        ptr_mapbuffer[i] = vec4(m_points[i].m_pos, 1.0f);
    }
    glUnmapBuffer(GL_ARRAY_BUFFER);
    // Bind point shader and render
    if (m_render_points)
    { 
        renderer::bind(*eff_points);
        glBindVertexArray(m_vao[0]);
        glUniformMatrix4fv(eff_points->get_uniform_location("V"), 1, GL_FALSE, value_ptr(V));
        glUniformMatrix4fv(eff_points->get_uniform_location("P"), 1, GL_FALSE, value_ptr(P));
        glDrawArrays(GL_POINTS, 0, m_points.size());
    }
    // Update and Render constraints
    if (m_render_lines) 
    {
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo[1]);
        // GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT | GL_MAP_INVALIDATE_RANGE_BIT
        vec4* ptr_mapbuffer = (vec4*)glMapBufferRange(GL_ARRAY_BUFFER, 0, LINES_SIZE,
            GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT);
        for (uint i = 0, j = 0; i < m_constraints.size(); ++i, j+=2)
        {
            ptr_mapbuffer[j] = vec4(m_constraints[i]->m_point1->m_pos, m_constraints[i]->GetStress());
            ptr_mapbuffer[j+1] = vec4(m_constraints[i]->m_point2->m_pos, m_constraints[i]->GetStress());
        }
        glUnmapBuffer(GL_ARRAY_BUFFER);
        // Bind line shader and render
        renderer::bind(*eff_lines);
        glBindVertexArray(m_vao[1]);
        glUniformMatrix4fv(eff_lines->get_uniform_location("MVP"), 1, GL_FALSE, value_ptr(MVP));
        glDrawArrays(GL_LINES, 0, m_constraints.size()*2);
    }
    // Render softbody faces
    if (m_render_faces)
    {
        renderer::bind(*eff_faces);
        glBindVertexArray(m_vao[0]);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vbo[2]);
        glUniformMatrix4fv(eff_faces->get_uniform_location("MVP"), 1, GL_FALSE, value_ptr(MVP));
        glUniform3fv(eff_faces->get_uniform_location("eye_pos"), 1, value_ptr(cam_pos));
        renderer::bind(*mat, "mat");
        renderer::bind(*light, "light");
        glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, nullptr);
    }
}

bool SoftBody::AddConstraint(Point *p1, Point *p2, const CONSTRAINT_TYPES &type)
{
    // Sanity check - points cant be the same
    if (p1 == p2) { return false; }
    // Check wether contraint between the two points already exists
    for (size_t i = 0; i < m_constraints.size(); i++)
    {
        if ((p1 == m_constraints[i]->m_point1 && p2 == m_constraints[i]->m_point2) ||
            (p1 == m_constraints[i]->m_point2 && p2 == m_constraints[i]->m_point1))
        {
            return false;
        }
    }
    // Create constraint
    switch (type)
    {
    case CONSTRAINT_TYPES::LINEAR_CONSTRAINT:
        m_constraints.push_back(new LinearConstraint{ p1, p2 });
        break;
    case CONSTRAINT_TYPES::SQUARE_CONSTRAINT:
        m_constraints.push_back(new SquareConstraint{ p1, p2 });
        break;
    default:
#if DEBUG | _DEBUG
        __debugbreak();
#endif
        std::cerr << "LOG - Invalid constraint type" << type << std::endl;
        break;
    }
    return true;
}

void SoftBody::AddForce(const vec3 &force)
{
    // Add the same force to all point masses
    for (uint i = 0; i < m_points.size(); i++)
    {
        m_points[i].AddForce(force);
    }
}

void SoftBody::AddSpinForce(const float force, const vec3 &axis)
{
    mat4 spin = rotate(mat4(1.0f), half_pi<float>(), axis);
    for (uint i = 0; i < num_verts; i++)
    {
        vec4 f = vec4(m_points[num_verts].m_pos - m_points[i].m_pos, 1.0f) * spin;
        m_points[i].AddForce(normalize(vec3(f + epsilon<float>()))*force);
    }
}

