/*--------------------------------------------------------\
| File: CameraSystem.h                                    |
|x-------------------------------------------------------x|
| Details: Manages switching between cameras and their    |
| movement based on user input.                           |
|x-------------------------------------------------------x|
| Author:                              Andrius Jefremovas |
| Description:        Camera movement and post-processing.|
\--------------------------------------------------------*/
#pragma once

#include <graphics_framework.h>

struct CameraSystem
{
    // List of available camera types.
    enum CAMERA_TYPES
    {
        FREE_CAM = 0,
        ARC_CAM = 1,
    };
    CameraSystem();
    ~CameraSystem();
    // Controls the active camera based on users input.
    void ProcessInput(float dt);
    // Returns camera which is currently in use.
    const graphics_framework::camera* GetActiveCamera();
private:
    CAMERA_TYPES m_active_cam; // Active camera type
    graphics_framework::free_camera m_free_cam;
    graphics_framework::arc_ball_camera m_arc_cam;
    bool m_lock_mouse;  // Mouse capture enable/disable
    float m_aspect;     // Screen aspect ratio
    // Free-cam parameters for mouse movement to camera rotation
    double m_cursor_x, m_cursor_y;         // X-Y cursor position             
    double m_current_x, m_current_y;       // X-Y current cursor position
    double m_delta_x, m_delta_y;           // X-Y amount the cursor moved
    double m_ratio_width, m_ratio_height;  // Screen width-height ratio

    void UpdateFreeCam(float dt);
    void UpdateArcCam(float dt);
};