#version 430

// Incoming data
layout (location = 0) in vec4 position;
layout (location = 1) in vec4 normals;

// Outgoing data
layout (location = 0) out vec3 out_position;
layout (location = 1) out vec3 out_normal;

void main()
{
    // Compute world position
	gl_Position = position;
	out_position = vec3(position);
    out_normal = vec3(normals);
}