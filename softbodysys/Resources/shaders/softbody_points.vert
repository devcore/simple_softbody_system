#version 430

/* Incomming */
uniform mat4 V;
layout (location = 0) in vec4 position;

void main()
{
    // Transform position into camera space
    gl_Position = V * position, 1.0f;
}