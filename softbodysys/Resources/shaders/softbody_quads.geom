#version 430

/* Incomming */
uniform mat4 MVP;
layout (lines_adjacency) in;
layout (location = 0) in vec3 position[];
layout (location = 1) in vec3 normals[];

/* Outgoing */
//layout (line_strip, max_vertices = 12) out;
layout (triangle_strip, max_vertices = 12) out;
layout (location = 0) out vec3 out_position;
layout (location = 1) out vec3 out_normal;
layout (location = 3) out vec4 out_colour;

void main()
{
    vec4 transformed_pos[4] = 
    {
        MVP * vec4(position[0], 1.0f),
        MVP * vec4(position[1], 1.0f),
        MVP * vec4(position[2], 1.0f),
        MVP * vec4(position[3], 1.0f)
    };
        
    vec3 normals2[2] = 
    {
        normalize(cross(position[1]-position[0], position[2]-position[0])),
        normalize(cross(position[2]-position[3], position[1]-position[3]))
    };
    
    vec3 accum_normal = normalize((normals2[0]+normals2[1])*0.5f);
    // Pass throught to fragment shader
    for(uint i = 0; i < gl_in.length(); i++)
    {
        gl_Position = transformed_pos[i];
        out_position = position[i];
        out_normal = accum_normal;
        //out_normal = normals[i];
        //out_colour = vec4(1.0f);
        EmitVertex();
    }
    EndPrimitive();
    
    //vec3 center = (position[0]+position[1]+position[2])/3.0f;
    //vec3 center2 = (position[1]+position[2]+position[3])/3.0f;
    //
    //gl_Position = MVP * vec4(center, 1.0f);
    //out_colour = vec4(1.0f, 0.0f, 0.0f, 1.0f);
    //EmitVertex();
    //gl_Position = MVP * vec4(center+normals[0]*0.3f, 1.0f);
    //out_colour = vec4(0.0f, 1.0f, 0.0f, 1.0f);
    //EmitVertex();  
    //EndPrimitive();
    //
    //gl_Position = MVP * vec4(center2, 1.0f);
    //out_colour = vec4(1.0f, 0.0f, 0.0f, 1.0f);
    //EmitVertex();
    //gl_Position = MVP * vec4(center2+normals[1]*0.2f, 1.0f);
    //out_colour = vec4(0.0f, 1.0f, 0.0f, 1.0f);
    //EmitVertex();  
    //EndPrimitive();
    
    //for(uint i = 0; i < gl_in.length(); i++)
    //{
    //    gl_Position = MVP * vec4(position[i], 1.0f);
    //    out_colour = vec4(1.0f, 0.0f, 0.0f, 1.0f);
    //    EmitVertex();
    //    gl_Position = MVP * vec4(position[i]+normals[0]*0.4f, 1.0f);
    //    out_colour = vec4(1.0f, 0.0f, 0.0f, 1.0f);
    //    EmitVertex();  
    //    EndPrimitive();
    //}
    //
    //for(uint i = 0; i < gl_in.length(); i++)
    //{
    //    gl_Position = MVP * vec4(position[i], 1.0f);
    //    out_colour = vec4(0.0f, 1.0f, 0.0f, 1.0f);
    //    EmitVertex();
    //    gl_Position = MVP * vec4(position[i]+normals[i]*0.8f, 1.0f);
    //    out_colour = vec4(0.0f, 1.0f, 0.0f, 1.0f);
    //    EmitVertex();  
    //    EndPrimitive();
    //}
    
}