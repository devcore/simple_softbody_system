#version 430

// Incoming data
uniform mat4 MVP;
layout (location = 0) in vec4 in_position_stress;

// Outgoing data
layout (location = 0) out float out_stress;

void main()
{
	// Calculate screen position of vertex
	gl_Position = MVP * vec4(in_position_stress.xyz, 1.0);
	// Output constraint stress
	out_stress = in_position_stress.w;
}