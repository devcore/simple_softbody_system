#version 430

// Directional light data
struct directional_light
{
	vec4 ambient_intensity;
	vec4 light_colour;
	vec3 light_dir;
};

// Material data
struct material
{
	vec4 emissive;
	vec4 diffuse_reflection;
	vec4 specular_reflection;
	float shininess;
};

uniform directional_light light; // Light data
uniform material mat;            // Material data
uniform vec3 eye_pos;            // Camera position
uniform sampler2D tex;           // Texture

// Incoming data
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 tex_coord;
layout (location = 3) in vec4 in_colour;

// Outgoing colour
layout (location = 0) out vec4 colour;

void main()
{
    // Ambient
	vec4 ambient = mat.diffuse_reflection * light.ambient_intensity;
	// Diffuse
    vec4 diffuse = (mat.diffuse_reflection * light.light_colour) * max(dot(normal, light.light_dir), 0);
	// View direction for specular
    vec3 view_dir = normalize(eye_pos - position);
	vec3 half_vector = normalize(light.light_dir + view_dir);
    // Specular
    vec4 specular = (mat.specular_reflection * light.light_colour) * pow(max(dot(normal, half_vector), 0), mat.shininess);
	// Texture
    vec4 tex_colour = texture(tex, tex_coord);
	colour = mat.emissive + ambient + diffuse;
	colour *= tex_colour + specular;
    //colour = in_colour;
    colour.xyz = normal;
	colour.a = 1.0f;
}