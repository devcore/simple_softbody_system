#version 430

// Incoming data
layout (location = 0) in vec4 color;

// Outgoing data
layout (location = 0) out vec4 colour;

void main()
{
    // Just set the colour
    colour = color;
}