#version 430

uniform mat4 P;
/* Incomming */
layout (points) in;
/* Outgoing */
layout (triangle_strip, max_vertices = 4) out;
layout (location = 0) out vec4 color;

// QUAD
vec3 quad[4] = 
{
    vec3( 0.1f,  0.0f, 0.0f),
    vec3( 0.0f,  0.1f, 0.0f),
    vec3( 0.0f, -0.1f, 0.0f),
    vec3(-0.1f,  0.0f, 0.0f),
};

void main()
{
    vec3 position = gl_in[0].gl_Position.xyz;
    // Emit the quad billboard
    gl_Position = P * vec4(position+quad[0], 1.0f);
    color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
    EmitVertex();
    gl_Position = P * vec4(position+quad[1], 1.0f);
    color = vec4(1.0f, 0.5f, 0.0f, 1.0f);
    EmitVertex();
    gl_Position = P * vec4(position+quad[2], 1.0f);
    color = vec4(0.8f, 0.8f, 0.0f, 1.0f);
    EmitVertex();
    gl_Position = P * vec4(position+quad[3], 1.0f);
    color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
    EmitVertex();
    EndPrimitive();
}