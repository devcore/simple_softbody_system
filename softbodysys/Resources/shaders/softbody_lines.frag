#version 430

// Incoming vertex colour
layout (location = 0) in float in_stress;

// Outgoing pixel colour
layout (location = 0) out vec4 out_colour;

void main()
{
	// Scale the amount of stress green to red
    const float R = in_stress*2.0f;
    const float G = (1.0f - in_stress*2.0f);
	out_colour = vec4(R, G, 0.1f, 1.0f);
}