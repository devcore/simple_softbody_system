#version 430

uniform mat4 MVP;

// Incoming data
layout (location = 0) in vec4 position;
layout (location = 1) in vec4 normals;

// Outgoing data
layout (location = 0) out vec3 out_position;
layout (location = 1) out vec3 out_normal;

void main()
{
	// Calculate screen position of vertex
	gl_Position = MVP * position;
    // Pass to fragment shader
	out_position = vec3(position);
    out_normal = vec3(normals);
}