#version 430

// Incoming data
uniform sampler2D tex;
uniform float opacity = 1.0f;
layout (location = 0) in vec2 tex_coord;

// Outgiong data
layout (location = 0) out vec4 out_colour;

void main()
{
	// Set out colour to sampled texture colour
	out_colour = texture(tex, tex_coord);
    out_colour.a = opacity;
}